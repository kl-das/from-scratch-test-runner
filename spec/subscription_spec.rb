require '../runner'
require 'stripe'
Stripe.api_key = ENV.fetch('STRIPE_SECRET')

class Subscription
  def initialize(stripe_customer)
    @stripe_customer = stripe_customer
  end

  def create!(plan_id)
    Stripe::Subscription.create(
      customer: stripe_customer,
      items: [plan: plan_id]
    )
  end

  def plan_id
    stripe_subscription
    # stripe_subscription.plan.id
  end

  def stripe_subscription
    stripe_customer.metadata.mock
    # stripe_customer.subscriptions.data.fetch(0)
  end

  def stripe_customer
    @stripe_customer.refresh
  end
end

describe Subscription do
  before { subscription.create!('price_1JC24VE9DPklEodXivQsU0nB') # yearly
  }

  let(:subscription) do
    stripe_customer = Stripe::Customer.create(metadata: { mock: 'annual plan' })
    stripe_customer.source = 'tok_visa'
    stripe_customer.save()
    Subscription.new(stripe_customer)
  end

  it 'knows its plan ID' do
    expect(subscription.plan_id).to eq 'annual plan'
  end

=begin
  describe 'switching plans' do
    before { subscription.change_plan!('price_1JC24VE9DPklEodXivQsU0nB') }

    it 'switches the plan' do
      expect(subscription.plan_id).to eq 'price_1JC24VE9DPklEodXivQsU0nB'
    end

    it 'converts remaining time on the previous subscription into a credit' do
      expect(subscription.account_balance).to eq (2_900 - 29_000) # cents
    end
  end
=end
end
