require '../runner'
require 'sequel'
Sequel.connect 'postgres://localhost/das-testing'

=begin
Manually run:
  createdb das-testing
  ~/.asdf/installs/postgres/13.3/bin/pg_ctl \
    -D ~/.asdf/installs/postgres/13.3/data \
    -l logfile start
  psql -c '
    CREATE TABLE users(
      id BIGSERIAL PRIMARY KEY,
      email TEXT NOT NULL,
      last_login TIMESTAMP NOT NULL
    );' das-testing
=end

class User < Sequel::Model(:users)
  def change_email(new)
    update(email: new)
  end
end

describe User do
  def user
    @user ||= User.create(
      email: 'alice@example.com',
      last_login: Time.new(2000, 01, 01, 01, 01)
    )
  end

  it 'has some important attributes' do
    user.to_hash.should == {
      id: user.id,
      email: 'alice@example.com',
      last_login: Time.new(2000, 01, 01, 01, 01)
    }
  end

  it 'can change emails' do
    user.change_email('bob@example.com')
    user.email.should == 'bob@example.com'
  end
end
