require '../runner'

describe 'describe' do
  describe 'nested describe' do
    it 'can still run tests' do
      expect(1 + 1).to eq 2
    end
  end
end

describe 'it' do
  it 'should not allow nested its' do
    expect do
      it 'should not work'
    end.to raise_error(NameError)
  end
end

describe 'expectations' do
  it 'can expect values' do
    expect(1 + 1).to(eq(2))
  end

  it 'can expect exceptions' do
    expect do
      raise ArgumentError.new
    end.to raise_error(ArgumentError)
  end

  it 'can pass after failing' do
    expect(1 + 2).to(eq(3))
  end
end

describe 'let' do
  let(:five) { 5 }
  let(:six) { five + 1 }
  let(:random) { rand }

  it 'is available inside the tests' do
    expect(five).to eq(5)
  end

  it 'always returns the same object' do
    expect(random).to eq(random)
  end

  it 'still fails when methods do not exist' do
    expect { non_existant_method }.to raise_error(NameError)
  end

  it 'can reference other lets' do
    expect(six).to eq 6
  end

  describe 'nested describe' do
    let(:sibling) { 'sibling' }
    it 'can reference lets from parent describe' do
      expect(five).to eq 5
    end
  end

  describe 'nested describe' do
    it 'can not see sibling lets' do
      expect { sibling }.to raise_error(NameError)
    end
  end
end

describe 'before' do
  before { @five = 5 }
  before { @six = @five + 1 }

  it 'should be visible inside the tests' do
    expect(@five).to eq 5
  end

  it 'can reference values set up by another before' do
    expect(@six).to eq 6
  end

  describe 'nested describe' do
    before { @sibling =  'sibling' }
    it 'can reference before from parent describe' do
      expect(@five).to eq 5
    end
  end

  describe 'nested describe' do
    it 'can not see sibling befores' do
      expect { sibling }.to raise_error(NameError)
    end
  end
end

describe 'interaction of let and before' do
  let(:five) { 5 }
  before { @six = five + 1 }
  let(:seven) { @six + 1 }

  it 'allows befores to reference lets' do
    expect(@six).to eq 6
  end

  it 'allows lets to reference befores' do
    expect(seven).to eq 7
  end
end
