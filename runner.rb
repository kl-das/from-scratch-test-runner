GREEN = "\e[32m"
RED   = "\e[31m"
RESET = "\e[30m"

# RSpec-like method on main:Object

def describe(description, &block)
  Describe.new(description, block).run
end

class Describe
  def initialize(description, block, befores = [], lets = {})
    @description = description
    @befores = befores
    @lets = lets
    @block = block
  end

  def describe(description, &block)
    Describe.new(description, block, @befores.dup, @lets.dup).run
  end

  def before(&block)
    @befores << block
  end

  def let(name, &block)
    @lets[name] = block
  end

  def it(description, &block)
    It.new(description, @befores, @lets, block).run
  end

  # [ ] Hide the it method from the test code
  def run
    puts @description
    instance_eval(&@block)
  end
end

class It
  def initialize(description, befores, lets, block)
    @description = description
    @befores = befores
    @lets = lets
    @lets_cache = {}
    @block = block
  end

  def run
    begin
      $stdout.write "  - #{@description}"
      @befores.each { |block| instance_eval(&block) }
      instance_eval(&@block)
      puts " (#{GREEN}OK#{RESET})"
    rescue Exception => e
      puts " (#{RED}fail#{RESET})"
      puts [
             "#{RED}* Backtrace#{RESET} (innermost line at bottom):",
             e.backtrace.reverse.map { |line| "#{RED}|#{RESET} #{line}" },
             "#{RED}* #{e}#{RESET}",
           ].flatten.map { |line| "\t#{line}" }.join("\n")
    end
  end

  def method_missing(name, *args)
    if @lets_cache.key?(name)
      @lets_cache.fetch(name)
    else
      value = instance_eval(&@lets.fetch(name) { super })
      @lets_cache[name] = value
      value
    end
  end

  def expect(actual = nil, &block)
    Actual.new(actual || block)
  end

  def eq(expected)
    Expectations::Equal.new(expected)
  end

  def raise_error(exception_class)
    Expectations::Error.new(exception_class)
  end
end


# Assertion plumbing

class Actual
  def initialize(actual)
    @actual = actual
  end

  def to(expectation)
    expectation.run(@actual)
  end
end

class Expectations
  class Equal
    def initialize(expected)
      @expected = expected
    end

    def run(actual)
      unless actual == @expected
        raise AssertionError.new(
          "Expected: #{@expected.inspect} but got #{actual.inspect}"
        )
      end
    end
  end

  class Error
    def initialize(exception_class)
      @exception_class = exception_class
    end

    def run(actual_block)
      begin
        actual_block.call
      rescue @exception_class
        return
      rescue StandardError => e
        raise AssertionError.new(
          format(
            "Expected to see error %s, but saw %s",
            @exception_class.inspect,
            e.inspect
          ))
      end

      raise AssertionError.new(format(
          "Expected to see error %s, but saw nothing",
          @exception_class.inspect
        ))
    end
  end
end

class AssertionError < RuntimeError
end
